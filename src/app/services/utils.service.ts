import { Inject, Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import * as cryptojs from "crypto-js";
import { TranslateService } from './../utils/translate/translate.service';
import { secretToken } from './../utils/values';
import {DOCUMENT} from "@angular/common";
import * as Color from "color";

@Injectable({
    providedIn: "root"
})
export class UtilsService {
    constructor(
        private translateService: TranslateService,
        @Inject(DOCUMENT) private document: Document
    ) {
    }

    public hMacSha512(text: string): string {
        try {
            let hash = cryptojs.HmacSHA512(text, secretToken.SECRET_PUBLIC);
            return hash.toString();
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public encrypt(text: string): string {
        try {
            let ciphertext = cryptojs.AES.encrypt(JSON.stringify(text), secretToken.SECRET_PUBLIC);
            return ciphertext;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public decrypt(ciphertext: any): string {
        try {
            let bytes = cryptojs.AES.decrypt(ciphertext.toString(), secretToken.SECRET_PUBLIC);
            var decryptedData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));
            return decryptedData;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + ciphertext);
            return "";
        }
    }

    public getOnlyNumbers(value) {
        if (value) {
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) { console.log(e); }
        }
        return null;
    };

    public getMsgStatusError(status) {
        let msg: string = this.translateService.getValue("statusDefault");
        switch (status) {
            case 0: msg = this.translateService.getValue("status0"); break;
            case 302: msg = this.translateService.getValue("status302"); break;
            case 304: msg = this.translateService.getValue("status304"); break;
            case 400: msg = this.translateService.getValue("status400"); break;
            case 401: msg = this.translateService.getValue("status401"); break;
            case 403: msg = this.translateService.getValue("status403"); break;
            case 404: msg = this.translateService.getValue("status404"); break;
            case 405: msg = this.translateService.getValue("status405"); break;
            case 410: msg = this.translateService.getValue("status410"); break;
            case 500: msg = this.translateService.getValue("status500"); break;
            case 502: msg = this.translateService.getValue("status302"); break;
            case 503: msg = this.translateService.getValue("status302"); break;
            default: break;
        }
        return msg;

    };

    public getItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };

    public getCountDuplicateList = (lista, value, campo) => {
        let count = 0;
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    count++;
                }
            }
        }
        return count;
    };

    public removeItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };

    public filterItemList = (lista, value: string, campo: string) => {
        if (!campo) { campo = "id" };
        if (!value) return lista;
        return lista.filter(item => {
            return item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
        });
    };

    public changeItemList(oldList: Array<any>, newList: Array<any>, item: any) {
        if (oldList && newList) {
            let index = oldList.indexOf(item);
            if (index > -1) {
                oldList.splice(index, 1);
            }
            newList.push(item);
        }
    }

    public getValueObjectField(obj: object, field: string) {
        if (field.includes(".")) {
            let fields = field.split(".");
            fields.forEach((fieldSplit) => {
                obj = obj[fieldSplit];
            });
            return obj;
        }
        return obj[field] || "";
    }

    public createValidator(func: any, msg: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: string } | null => {
            if (!func(control.value)) {
                msg = this.translateService.getValue(msg);
                return { 'msgErro': msg };
            }
            return null;
        };
    }

    public getFilterPagination(filter) {
        let newFilter: string = "?";
        let page = (filter.page) ? "page=" + filter.page : "&page=1";
        let limit = (filter.limit) ? "&limit=" + filter.limit : "&limit=10";
        let asc = (filter.asc) ? "&asc=" + filter.asc : "";
        let desc = (filter.desc) ? "&desc=" + filter.desc : "";
        newFilter += page + limit + asc + desc;
        return newFilter;
    }

    public showDialog(title: string, msg: string, buttons: Array<any>, icon: string, width: string) {
        if (!title) title = "MENSAGEM";
        let data = {
            "title": title,
            "msg": msg,
            "btns": buttons,
            "icon": icon,
            "width": width
        }
        // let dialogRef = this.dialog.open(DialogComponent, {"data": data});
        return null;
    }

    public setColorTheme(primary: string, secundary, tertiary) {
        let colorTheme = this.cssGenerator(primary, secundary, tertiary);
        this.setGlobalCss(colorTheme);
    }

    private setGlobalCss(css: string) {
        this.document.documentElement.style.cssText = css;
        console.log(this.document.documentElement.style.cssText);
    }

    private getContrast(color, ratio = 0.8){
        let colorC = Color(color);
        console.log(`IsDark: ${colorC.isDark()}`)
        console.log(`IsDark: ${colorC.whiten(1)}`)
        return colorC.isDark() ? colorC.mix(Color("white"), 0.8): colorC.blacken(ratio);
    }

    private cssGenerator(primaryColor: string, secundaryColor: string, tertiaryColor: string){
        let ratioTint = 0.1;
        let ratioShade = 0.1;
        let ratioSecondary = 0.4;
        let ratioTertiary = 0.3;
        let primary = Color(primaryColor);
        let secondary = secundaryColor ? Color(secundaryColor) :primary.lighten(ratioSecondary);
        // secondary = secondary.saturate(0.8);
        let tertiary = tertiaryColor ? Color(tertiaryColor) : primary.darken(ratioTertiary);
        return `
            --ion-color-primary: ${primary};
            --ion-color-primary-contrast: ${this.getContrast(primary)};
            --ion-color-primary-shade: ${primary.darken(ratioShade)};
            --ion-color-primary-tint: ${primary.lighten(ratioTint)};
            --ion-color-secondary: ${secondary};
            --ion-color-secondary-contrast: ${this.getContrast(secondary)};
            --ion-color-secondary-shade: ${secondary.darken(ratioShade)};
            --ion-color-secondary-tint: ${secondary.lighten(ratioTint)};
            --ion-color-tertiary: ${tertiary};
            --ion-color-tertiary-contrast: ${this.getContrast(tertiary)};
            --ion-color-tertiary-shade: ${tertiary.darken(ratioShade)};
            --ion-color-tertiary-tint: ${tertiary.lighten(ratioTint)};
            --background-primary: linear-gradient(181deg, rgba(255,255,255,1) 0%, rgba(247,248,253,1) 3%, rgba(14,37,207,1) 100%);
        `;
    }
}