import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { URLs } from '../utils/values';

@Injectable({
    providedIn: 'root'
})
export class AcaoService {
    constructor(private httpClient: HttpClient) { }
    
    public list(filtros: any){
        return  this.httpClient.get(environment.API + URLs.ACAO + filtros);
    }
}