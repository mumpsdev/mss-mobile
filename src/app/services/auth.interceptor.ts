import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {tap, finalize} from "rxjs/operators";
import { 
    HttpEvent, 
    HttpInterceptor, 
    HttpHandler, 
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
// import {Observable}  from 'rxjs/Observable';
import {Observable} from "rxjs"
import { AppComponent } from '../app.component';
import { secretToken } from '../utils/values';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private app: AppComponent,
    ) {
       
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("chegou!");
        let ok: string;
        let newReq = this.setTokenSessionStory(req);
        return next.handle(newReq).pipe(
            tap(
                event => { 
                    ok = event instanceof HttpResponse ? 'succeeded' : ''
                }, error => {
                    ok = 'failed';
                    console.log(error);
                }
            ),finalize(() => {})
          );
    }

    private setTokenSessionStory(req: HttpRequest<any>): HttpRequest<any>{
        let token = sessionStorage.getItem(secretToken.TOKEN);
        if(token && token != "undefined"){
            let newReq = req.clone({
                headers: req.headers.set(secretToken.TOKEN, token)
            });
            return newReq;
        }else{
            return req;
        }
    }
    
    private saveTokenSessionStory(res: HttpResponse<any>): void{
        let token = res.headers[secretToken.TOKEN]
        if(token){
            sessionStorage.setItem(secretToken.TOKEN, token);
        }
    }
}