import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { URLs } from '../utils/values';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  public getUserNameUserLogged(login: string){
    return this.httpClient.get(`${environment.API}/api/v1/loginAuth/${login}`);
  }

  public login(user: any){
    return this.httpClient.post(environment.API + URLs.AUTH_AUTHENTICATE, user)
  }
}
