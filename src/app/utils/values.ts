export const URLs = {//Urls rest.
     "AUTH_LOGIN": "/api/v1/loginAuth/:login",
     "AUTH_AUTHENTICATE": "/api/v1/authenticate",
     "AUTH_ALL": "/*",
     //Usuário
     "USUARIO": "/api/v1/usuario",
     "USUARIO_ID": "/api/v1/usuario/:id",
     "USUARIO_PAGINATION": "/api/v1/usuario/pagination",
     "USUARIO_FILTER": "/api/v1/usuario/filter",
     //Organizacao
     "ORGANIZACAO": "/api/v1/organizacao",
     "ORGANIZACAO_ID": "/api/v1/organizacao/:id",    
     //Filial
     "FILIAL": "/api/v1/filial",
     "FILIAL_ID": "/api/v1/filial/:id",     
     //Perfil
     "PERFIL": "/api/v1/perfil",
     "PERFIL_ID": "/api/v1/perfil/:id",    
     //Acao
     "ACAO": "/api/v1/acao",
     "ACAO_NOTIN": "/api/v1/acao/notin",
     "ACAO_ID": "/api/v1/acao/:id",
     //Tipo Documento
     "TIPO_DOCUMENTO": "/api/v1/tipodocumento",
     "TIPO_DOCUMENTO_ID": "/api/v1/tipodocumento/:id", 
     //Tipo Contato
     "TIPO_CONTATO": "/api/v1/tipocontato",
     "TIPO_CONTATO_ID": "/api/v1/tipocontato/:id",  
     //Contato
     "CONTATO": "/api/v1/contato",
     "CONTATO_ID": "/api/v1/contato/:id",
     //Documento
     "DOCUMENTO": "/api/v1/documento",
     "DOCUMENTO_ID": "/api/v1/documento/:id",
     //Feriado
     "FERIADO": "/api/v1/feriado",
     "FERIADO_ID": "/api/v1/feriado/:id"    
}

export const typeMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const objectMsg = {//Objetos de retorno
    "OBJ": "obj",
    "LIST": "list",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "LIMIT": "limit",
    "RANGE_INIT": "rangeInit",
    "RANGE_END": "rangeEnd",
    "ASC": "asc",
    "DESC": "desc",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-access-token",
    "SECRET_PUBLIC" : "projeto-publico"
}