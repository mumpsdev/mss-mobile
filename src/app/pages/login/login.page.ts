import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userLogged: any;

  constructor(
  ) {}

  ngOnInit() {
  }
  
  public proximo(){
    this.userLogged = {nome: "Usuário teste"};
  }

  public voltar(){
    this.userLogged = {};
  }

}
