import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { secretToken } from './utils/values';
import { UtilsService } from './services/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  private usuario: any;
  private links: Array<any> = [];

  // public appPages = [
  //   {
  //     title: 'Home',
  //     url: '/home',
  //     icon: 'home'
  //   }
  // ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navController: NavController,
    private utilsService: UtilsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (!this.usuario) {
        this.logout();
      }
      console.log("iniciando!");
      this.utilsService.setColorTheme("#252f80", "#CE93D8", "#FFFF00");
    });
  }

  public setUser(obj: any) {
    let usuario = obj["usuario"];
    if (obj[secretToken.TOKEN] && obj[secretToken.TOKEN] != "undefined") {
      sessionStorage.setItem(secretToken.TOKEN, obj[secretToken.TOKEN]);
      this.navController.navigateRoot("/home");
    }
    if (usuario) {
      this.usuario = usuario;
      this.links = this.createLinks(usuario)
    }
  }

  public createLinks(usuario: any): Array<any> {
    let links = [];
    let perfis = usuario["perfis"];
    if (perfis) {
      perfis.forEach(perfil => {
        let acoes = perfil["acoes"];
        if (acoes) {
          acoes.forEach(acao => {
            if (acao["menu"]) {
              let menu = links.find((l) => l["menu"] == acao["menu"]);
              if (menu) {
                menu["links"].push({ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] })
              } else {
                links.push({ "menu": acao["menu"], "links": [{ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] }] });
              }
            } else if (acao["rota"]) {
              links.push({ "nome": acao["nome"], "rota": acao["rota"], "icon": acao["icon"] });
            }
          });
        }
      });
    }
    return links;
  }

  public logout() {
    this.usuario = null;
    sessionStorage.removeItem(secretToken.TOKEN);
    this.navController.navigateRoot("/login");
  }
}
